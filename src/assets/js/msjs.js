
function cargar(param) {
	var div = document.getElementById('divContent').value;
	 $('#'+div).load(param);
}

function primera() {
	var primera = document.getElementById('primera').value;
	cargar(primera);
}

function anterior() {
	var anterior = document.getElementById('anterior').value;
	cargar(anterior);
}

function siguiente() {
	var siguiente = document.getElementById('siguiente').value;
	cargar(siguiente);
}

function ultima() {
	var ultima = document.getElementById('ultima').value;
	cargar(ultima);
}

function cargarPagina(pagina) {
	cargarContenido('headerContent', '../comunes/layoutMsjs.html #header');
	cargarContenido('footerContainer', '../comunes/layoutMsjs.html #footer');
	cargarMenu(pagina);
}

function cargarContenido(donde, que) {
	var content = '#' + donde;
	$(content).load(que);
}

function cargarMenu(opcionSelected) {
	$('#sidebar').load('/vatar/comunes/menu.html', function(){
		$('#div'+opcionSelected).addClass("ThmBgHeader");
		$('#'+opcionSelected).addClass("FrameLink");
		$('#'+opcionSelected).removeClass("NavLink");	
    });
}