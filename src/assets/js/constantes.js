/**
 * 
 */
var cssMsn = '<link rel="stylesheet" type="text/css" href="/vatar/css/msn.css">';
var cssEstilos = '<link class="cssfx" rel="stylesheet" type="text/css" href="/vatar/css/estilos.css">';


var scriptCssfx = '<script type="text/javascript" src="/vatar/js/cssfx.js"></script>';
var scriptJquery = '<script type="text/javascript" src="/vatar/js/jquery-3.1.1.js"></script>';
var scriptVatar = '<script type="text/javascript" src="/vatar/js/vatar.js"></script>';
var scriptMsjs = '<script type="text/javascript" src="/vatar/js/msjs.js"></script>';

function loadHeader(pagina, vars) {
	var head = $('head');
	head.append(cssMsn).append(cssEnlaces).append(cssEstilos).append(
		scriptCssfx).append(scriptJquery).append(scriptVatar);
	if (vars != null) {
		for (i = 0; i < vars.length; i++) {
			head.append(vars[i]);
		}
	}
}