var cssEscuelas = '<link rel="stylesheet" type="text/css" href="/vatar/css/escuelas.css">';
var cssAgentes = '<link rel="stylesheet" type="text/css" href="/vatar/css/agentes.css">';
var cssMensajes = '<link rel="stylesheet" type="text/css" href="/vatar/css/mensajes.css">';

var agentesPerfil = new Array("achus", "andrella", "buffy", "cassie", "cookie",
		"crysania", "danielle", "ginny", "leonita", "marina", "mirelle",
		"naya", "rory");

var agentesSbfc = new Array("buffy", "naya", "cassie", "irene", "ariel",
		"ladyangelina", "leonita", "miaka", "andrella", "ginny");
var nicksSBFC = new Array("_Buffy_Ane_Summers_", "naya_enferma", "Casie4194",
		"Babylover444", "kira_xix", "LadyAngelinaJ", "Leonita19811", "Miaka_Black",
		"Andrella-de-Alfehim", "^Ginny^");

var agentesVatar = new Array("buffy", "naya", "cassie", "cookie", "ariel",
		"ladyangelina", "leonita", "miaka", "andrella", "ginny", "rory", "marina");
var nicksVATAR = new Array("_Buffy_Ane_Summers_", "naya_enferma", "Casie4194",
		"Cookie", "kira_xix", "LadyAngelinaJ", "Leonita1981", "Miaka_Black",
		"Andrella-de-Alfehim", "Ginny&copy;", "Rory Black", "Marina de Potter");


function cargarPagina(pagina) {
	$('head').append('<link rel="shortcut icon" href="/vatar/images/favicon.ico" />')
			.append('<meta http-equiv="content-type" content="text/html; charset=UTF-8">')
			.append('<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">')
			.append('<link rel="stylesheet" type="text/css" href="/vatar/css/msn.css">')
			.append('<link rel="stylesheet" type="text/css" href="/vatar/css/estilos.css" class="cssfx">');
	if (pagina == 'agentes') {
		anadeHead(cssAgentes);
	}
	if (pagina == 'escuelas') {
		anadeHead(cssEscuelas);
	}
	if (pagina == 'mensajesSbfc' || pagina == 'mensajesVatar') {
		anadeHead(cssMensajes);
		cargarPerfiles(pagina);
		if (pagina == 'mensajesVatar') {
			cargarPaginador();
		}
	}
	cargarContenido('headerContent', '/vatar/comunes/layout.html #header');
	cargarContenido('footerContainer', '/vatar/comunes/layout.html #footer');
	cargarMenu(pagina);
}

function cargarListaAgentes(agente) {
	$("#contenidoLista").load("/vatar/comunes/listasMenu.html #listaAgentes",
			function() {
				$('#' + agente).addClass("agenteSelect");
			});
}

function cargarPerfilAgente(agente) {
	cargarContenido('divAgentes', '/vatar/agentes/perfil' + agente + '.html')
}

function cargarListaEscuelas(escuela) {
	$("#contenidoLista").load("/vatar/comunes/listasMenu.html #listaEscuelas",
			function() {
				$('#esc' + escuela).addClass("escuelaSelect");
			});
}

function cargarContenido(donde, que) {
	var content = '#' + donde;
	$(content).load(que);
}

function cargarMenu(opcionSelected) {
	$('#sidebar').load('/vatar/comunes/menu.html', function() {
		$("#div" + opcionSelected).addClass("ThmBgHeader");
		$("#" + opcionSelected).addClass("FrameLink");
		$("#" + opcionSelected).removeClass("NavLink");
	});
}

function anadeHead(meta) {
	$('head').append(meta);

}

function irA(destino) {
	window.location = destino;
}

var classActive = 'Command';
var classInactive = 'ThmFgInactiveText';
var urlFlechaBackInactiva = '/vatar/images/1arrow_back_b.gif';
var urlFlechaBackActiva = '/vatar/images/1arrow_back_1.gif';
var urlFlechaForwInactiva = '/vatar/images/1arrow_forward_b.gif';
var urlFlechaForwActiva = '/vatar/images/1arrow_forward_1.gif';

function initMensajes(url, pagActual, ultPagina, numMensajes) {
	if (pagActual > 1) {
		$(".paginadorMensajes div.primero").addClass(classActive);
		$(".paginadorMensajes a.primero img").attr("src", urlFlechaBackActiva);
		$(".paginadorMensajes a.primero").click(function() {
			window.location = url + '01.html';
		});

		$(".paginadorMensajes div.anterior").addClass(classActive);
		$(".paginadorMensajes a.anterior img").attr("src", urlFlechaBackActiva);
		$(".paginadorMensajes a.anterior").click(function() {
			window.location = url + zeroPad(pagActual - 1, 2) + '.html';
		});

	} else {
		$(".paginadorMensajes a.primero img")
				.attr("src", urlFlechaBackInactiva);
		$(".paginadorMensajes div.primero").addClass(classInactive);

		$(".paginadorMensajes a.anterior img").attr("src",
				urlFlechaBackInactiva);
		$(".paginadorMensajes div.anterior").addClass(classInactive)
	}

	if (pagActual < ultPagina) {
		$(".paginadorMensajes div.siguiente").addClass(classActive);
		$(".paginadorMensajes a.siguiente img")
				.attr("src", urlFlechaForwActiva);
		$(".paginadorMensajes a.siguiente").click(function() {
			window.location = url + zeroPad(pagActual + 1, 2) + '.html';
		});

		$(".paginadorMensajes div.ultimo").addClass(classActive);
		$(".paginadorMensajes a.ultimo img").attr("src", urlFlechaForwActiva);
		$(".paginadorMensajes a.ultimo").click(function() {
			window.location = url + zeroPad(ultPagina, 2) + '.html';
		});

	} else {
		$(".paginadorMensajes a.siguiente img").attr("src",
				urlFlechaForwInactiva);
		$(".paginadorMensajes div.siguiente").addClass(classInactive);

		$(".paginadorMensajes a.ultimo img").attr("src", urlFlechaForwInactiva);
		$(".paginadorMensajes div.ultimo").addClass(classInactive)
	}
	var index = ((pagActual - 1) * 30) + 2;
	var fin = (index + 29) < numMensajes ? (index + 29) : numMensajes;
	$(".ThmFgInactiveText.navigator.actual").text(
			"  " + index + " a " + fin + " de " + numMensajes + "  ");
}

function zeroPad(num, places) {
	var zero = places - num.toString().length + 1;
	return Array(+(zero > 0 && zero)).join("0") + num;
}

String.prototype.capitalize = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
}

function cargarPerfiles(pagina) {
	var imgNick = "De:  <img src=\"/vatar/images/cool_global_nick.gif\" class=\"imgCentrada\" width=\"15\" height=\"15\">";
	
	if (pagina == 'mensajesSbfc') {
		agentes = agentesSbfc;
		nicks = nicksSBFC;
	} else if (pagina == 'mensajesVatar') {
		agentes = agentesVatar;
		nicks = nicksVATAR;
	}
	for (i = 0; i < agentes.length; i++) {
		var agente = agentes[i];
		var text = imgNick + "<a class=\"irPerfil\"";
		
		// Si tiene perfil
		if (agentesPerfil.indexOf(agente) > -1) {
			text = text + "onclick=\"irA('/vatar/agentes/perfil" + agente + ".html');\"";
		}
		text = text + "> " + nicks[i] + "</a>"
		$(".msgDe" + agente.capitalize()).html(text);
	}

}
function cargarPaginador() {
	var text = "<div class=\"primero navigator\">"
		+ "<a class=\"primero flechaNav\" title=\"Ir a los primeros 15 mensajes de esta discusi&oacute;n\"> "
		+ "<img>Primer</a></div>"
	+ "<div class=\"anterior navigator\">"
		+ "<a class=\"anterior flechaNav\" title=\"Ir a los 15 mensajes anteriores de esta discusi&oacute;n\"> "
		+ "<img>Anterior</a></div>"
	+ "<div class=\"ThmFgInactiveText navigator actual\"></div>"
	+ "<div class=\"siguiente navigator\">"
		+ "<a class=\"siguiente flechaNav\" title=\"Ir a los 15 mensajes siguientes\"> Siguiente<img></a>&nbsp;</div>"
	+ "<div class=\"ultimo navigator\">"
		+"<a class=\"ultimo flechaNav\" title=\"Ir a los &uacute;ltimos 15 mensajes de esta discusi&oacute;n\"> &Uacute;ltimo<img></a>&nbsp;</div>";
	$(".paginadorMensajes").html(text);
}