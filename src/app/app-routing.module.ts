import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgentesComponent } from './agentes/agentes.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  {path: '' , component: AppComponent},
  {path: 'agentes' , component: AgentesComponent}  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
